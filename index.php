<?php
include('inc/config.php');
$error = "";


/*- Log in -*/
if (isset($_POST['u']) && isset($_POST['p'])){
	$request = "SELECT * FROM `users`";
	$result = $db->query($request);

	while($row = $result->fetch_assoc()){
		if ($_POST['u'] == $row['username'] && strcmp(crypt($_POST['p'], $row['salt']), $row['password'])){
			/*- Login was successful -*/
			$_SESSION['id'] = $row['id'];
			$_SESSION['u'] = $row['username'];
			$_SESSION['p'] = $row['permissions'];
			header('Location: files.php');
			die('Redirect: <a href="files.php">files.php</a>');
		}
	}

	$error = "Unauthorized.  Wrong username/password?"; /*- If the user didn't redirect, then we trigger the error -*/
}

?>

<html>
	<head>
		<title>Log In</title>
		<link rel="stylesheet" href="res/main.css" />
		<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
	</head>
	<body>
		<form class="login" method="POST" action="">
			<div class="bar">LOGIN</div>
			<p class="error"><?php echo $error; ?></p>
			<input type="text" name="u" placeholder="username" autocomplete="off" />
			<input type="password" name="p" placeholder="password" autocomplete="off" />
			<button>Submit</button>
		</form>
	</body>
</html>