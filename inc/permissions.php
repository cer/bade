<?php

/* check_authorized: int -> bool
 * check_authorized(l): Checks to see if the user is authorized by modding its
 * permission number by the prime number specified.  If the mod is 0, then it
 * returns true.  Else false.  Significant enough to be its own function.
 */
function checkauthorized($level){
	if (isset($_SESSION['p']) && ($_SESSION['p'] % $level) == 0){
		return true;
	} else {
		return false;
	}
}

/* authorized: int -> bool
 * authorized(l): Checkes to see if the user is allowed to be on the page.  If
 * the user is not allowed, then it redirects the user to the index, and kills
 * the rest of the script
 */
function authorized($level){
	if (!checkauthorized($level)){
		header('Location: index.php');
		die('Location <a href="index.php">index.php</a>');
		//die('session uid: '.$_SESSION['p'].' mod of the level: '.($_SESSION['p'] % $level)); //debugging
	}
	return true;
}

/*- Permission Constants -*/
$files = 2; /*- Can view and download files -*/
$logs  = 3; /*- Can veiw logs and check hashes for downloaded files -*/
$admin = 5; /*- Can create users, ban users, change permissions, and upload files -*/