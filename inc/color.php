<?php

/* A snippit of code shamelessly stolen from stack exchange.
 * https://stackoverflow.com/questions/3724111/how-can-i-convert-strings-to-an-html-color-code-hash
 * Works very well for my purposes.
 */

 function stringToColorCode($str) {
  $code = dechex(crc32($str));
  $code = substr($code, 0, 6);
  return $code;
}