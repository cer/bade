<?php
include('inc/config.php');
include('inc/permissions.php');
authorized($admin);

if (isset($_POST['u']) && isset($_POST['p'])){
	$query = $db->prepare('INSERT INTO `users` (username, password, salt, permissions) VALUES (?, ?, ?, ?)');
	$query->bind_param("sssi", $username, $password, $salt, $permissions);
	/*- Uname, pass, ect -*/
	$username = $_POST['u'];
	$salt     = openssl_random_pseudo_bytes(30);
	$password = crypt($_POST['p'], $salt);
	/*- Permissions -*/
	$permissions = pow(2, isset($_POST['psf'])) * pow(3, isset($_POST['psl'])) * pow(5, isset($_POST['psa']));
	$query->execute();
}

?>
<html>
	<head>
		<title>File Holder</title>
		<link rel="stylesheet" href="res/main.css" />
		<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
	</head>
	<body>
	<?php include('inc/nav.php'); ?>
	<main>
<form action="" method="POST">
	<input type="text" name="u" placeholder="Username" />
	<input type="password" name="p" placeholder="Password" />
	<p><input type="checkbox" name="psf" /> User Can Download Files</p>
	<p><input type="checkbox" name="psl" /> User Can Read Logs</p>
	<p><input type="checkbox" name="psa" /> User Can Administrate Site</p>
	<button>Register</button>
</form>
</main>
</body>
</html>