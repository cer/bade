<?php
include('inc/config.php');
include('inc/permissions.php');
include('inc/color.php');
authorized($admin);

if (isset($_POST['uid'])) {
	$permissions = pow(2, isset($_POST['psf'])) * pow(3, isset($_POST['psl'])) * pow(5, isset($_POST['psa']));
	$postrequest = $db->prepare("UPDATE `users` SET permissions='".$permissions."' WHERE id='".mysqli_real_escape_string($db, $_POST['uid'])."'");
	$postrequest->execute();
}

$request = "SELECT * FROM `users` ORDER BY id DESC";
$result = $db->query($request);
$buffer = "";

while($row = $result->fetch_assoc()){

	$F = ''; $L = ''; $A = '';

	if (($row['permissions'] % 2) == 0){
		$F = "checked";
	}
	if (($row['permissions'] % 3) == 0){
		$L = "checked";
	}
	if (($row['permissions'] % 5) == 0){
		$A = "checked";
	}

	$buffer .= "<tr>
		<td style='color: ".stringToColorCode($row['username']).";'>&#x2588;</td>
		<td>".htmlspecialchars($row['id'])."</td>
		<td>".htmlspecialchars($row['username'])."</td>
		<td>
			<form method='post' action=''>
				<input type='hidden' name='uid' value='".htmlspecialchars($row['id'])."' />
				<input type='checkbox' name='psf' ".$F." /> F
				<input type='checkbox' name='psl' ".$L." /> L
				<input type='checkbox' name='psa' ".$A." /> A
				<button>update</button>
			</form>
		</td>
	</tr>";
}
?>

<html>
	<head>
		<title>File Holder</title>
		<link rel="stylesheet" href="res/main.css" />
		<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
	</head>
	<body>
	<?php include('inc/nav.php'); ?>
	<main>
		<table>
			<tr>
				<th> </th>
				<th>uid</th>
				<th>User</th>
				<th>Permissions</th>
			</tr>
			<?php echo $buffer; ?>
		</table>
	</main>
	</body>
</html>