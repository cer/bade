<?php
include('inc/config.php');
include('inc/permissions.php');
include('inc/color.php');
authorized($logs);

$request = "SELECT * FROM `logs`";
$result = $db->query($request);
$buffer = "";

while($row = $result->fetch_assoc()){
	$buffer .= "<tr>
		<td style='color: ".stringToColorCode($row['uname']).";'>&#x2588;</td>
		<td>".htmlspecialchars($row['uname'])."</td>
		<td>".htmlspecialchars($row['uid'])."</td>
		<td>".htmlspecialchars($row['file'])."</td>
		<td>".htmlspecialchars($row['ip'])."</td>
		<td>".htmlspecialchars($row['useragent'])."</td>
		<td>".htmlspecialchars($row['stamp'])."</td>
		<td>".htmlspecialchars($row['hash'])."</td>
	</tr>";
}
?>

<html>
	<head>
		<title>File Holder</title>
		<link rel="stylesheet" href="res/main.css" />
		<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
	</head>
	<body>
	<?php include('inc/nav.php'); ?>
	<main>
		<table>
			<tr>
				<th> </th>
				<th>User</th>
				<th>uid</th>
				<th>File</th>
				<th>ip</th>
				<th>User Agent</th>
				<th>Timestamp</th>
				<th>Hash</th>
			</tr>
			<?php echo $buffer; ?>
		</table>
	</main>
	</body>
</html>