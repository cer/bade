# Bade

## What is it?
Bade is a very simple system that keeps files safe, or at least as safe as they possibly can be.  Users log into the web application, and download their files.  Each download is tracked, and an md5 hash is attached to the very end of the raw file.  Should a file turn up in a place it shouldn't be, the exact user that downloaded it can be traced out.  A permission system is cooked in with bade, although it is advisable you take the parts you want and imbed it into your own project.

## Setting it up:
Currently there is no install script for this.  I may work on that in the future.  For now you will have to install it manually.

First, go to the config.php file found in inc/ and update it with your mysql server information.
~~~~
/*- Database to connect to. -*/
/*- host, user, pass, db -*/
$db = new mysqli('', '', '', '');
~~~~

Next, choose a directory (that is prefferably not publically accessable), and put the path here:
~~~~
/*- Folder containing files -*/
$folder = "";
~~~~
Note: this path should be relative to the main folder, not to inc/.  Absolute paths probably work to.

After this, dump the tables in db.sql to your mysql database.  Log in with the default username and password (admin:admin).
![Login screen](https://i.imgur.com/TElssf4.png "Login screen")

From here you are all set.  Chances are you will want to make your own user account with a different name and password for this stuff.

## General Usage:

__Files:__  Holds the obvious, the files that bade will tag.  All of the files are served from gate.php, which I will explain later.

__Logs:__  The file upload and download logs can be seen from here.  The color coded bars on the side are color hashes of the usernames, and can help you find patterns in downloading more quickly.

__Users:__  Lists the users of the site, and allows anyone with admin permission to edit their permission values.  Again, read down further for an explaination as to how these exactly work.

__Upload:__  Uploads a file to the database.  The file will be stored (without a file ending) to the folder specified in the config.

__Register:__  Allows the admin to register accounts and give permissions.

__Logout:__  Logs the user out.

## Reading the tags on files:

Doing this is much simpler than you think.  First open up the suspect file in hxd or any hex editor.  Scroll down to the very bottom, and copy the last 32 characters.  This is an md5 hash.  Search for the md5 hash in logs to find the user who downloaded it.

## How does it work:

The core idea behind this system is that an md5 hash is attached to the end of each file, and corisponds with a log entry.  Log entries record ip addresses, user agents, ect.  The permission system is a fairly simple prime number system that uses mod to figure out if the user is allowed to access a part of the system.