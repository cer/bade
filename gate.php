<?php
include('inc/config.php');
include('inc/permissions.php');
authorized($files);

if (isset($_GET['id'])){
	$request = "SELECT * FROM `files` where id = ".mysqli_real_escape_string($db, intval($_GET['id']));
	$result = $db->query($request);
	while ($row = $result->fetch_assoc()){
		$loc   = $row['location'];
		$type  = $row['mime'];
		$fname = $row['filename'];
	}
	if (isset($loc)){
		$query = $db->prepare('INSERT INTO `logs` (uid, uname, file, ip, useragent, hash) VALUES (?, ?, ?, ?, ?, ?)');
		$query->bind_param('isisss', $uid, $uname, $file, $ip, $agent, $hash);
		$uid   = $_SESSION['id'];
		$uname = $_SESSION['u'];
		$file  = $_GET['id'];
		$ip    = $_SERVER['REMOTE_ADDR'];
		$agent = $_SERVER['HTTP_USER_AGENT'];
		$hash  = md5(time());
		$query->execute();

		header('Content-Description: File Transfer');
		header('Content-Type: '.$type);
		header('Content-Disposition: attachment; filename='.$fname);
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		readfile($folder."/".$loc);
		echo ' '.base64_encode($_SESSION['id']);
		exit;
	}
}else{
	header('Location: files.php');
	die('Location: <a href="files.php">files.php</a>');
}