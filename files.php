<?php
include('inc/config.php');
include('inc/permissions.php');
authorized($files);

$request = "SELECT * FROM `files`";
$result = $db->query($request);
$buffer = "";

while($row = $result->fetch_assoc()){
	$buffer .= "<div class='file'><h2><a href='gate.php?id=".htmlspecialchars($row['id'])."'>".htmlspecialchars($row['name'])."</a></h2>
	<p>".htmlspecialchars($row['description'])."<p></div>";
}
?>
<html>
	<head>
		<title>File Holder</title>
		<link rel="stylesheet" href="res/main.css" />
		<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
	</head>
	<body>
	<?php include('inc/nav.php'); ?>
	<main>
		<?php echo $buffer; ?>
	</main>
	</body>
</html>