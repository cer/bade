<?php
include('inc/config.php');
include('inc/permissions.php');
authorized($admin);

$filetypes = array(
	"pdf" => "application/pdf",
	"odt" => "application/odt",
	"doc" => "application/msword",
	"docx" => "application/msword",
	"epub" => "application/epub+zip",
	"jpg" => "image/jpeg",
	"jpeg" => "image/jpeg",
	"png" => "image/png",
	"gif" => "image/gif",
	"exe" => "application/octet-stream",
	"zip" => "application/zip",
	"gz" => "application/tar+gzip",
	"rar" => "application/rar"
);

if (isset($_POST['n']) && isset($_POST['d'])){
	$hash = md5($_FILES['file']['name']);
	move_uploaded_file($_FILES['file']['tmp_name'], $folder."/".$hash);

	$query = $db->prepare("INSERT INTO `files` (name, location, description, mime, filename) VALUES (?, ?, ?, ?, ?)");
	$query->bind_param("sssss", $name, $location, $description, $mime, $filename);
	$name = $_POST['n'];
	$location = $hash;
	$description = $_POST['d'];
	$type = end(explode('.', $_FILES['file']['name']));
	$mime = $filetypes[$type];
	$filename = $_FILES['file']['name'];/*- This technically shouldn't ever be done, and will throw a strict standards notice.  But it Just Werks�. -*/
	$query->execute();

	header('Location: files.php');
}

?>
<html>
	<head>
		<title>File Holder</title>
		<link rel="stylesheet" href="res/main.css" />
		<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
	</head>
	<body>
	<?php include('inc/nav.php'); ?>
	<main>
<form class="fileupload" method="post" enctype="multipart/form-data" action="">
	<p><input type="input" name="n" placeholder="Title" /></p>
	<p><input type="file" name="file" /></p>
	<p><input type="input" name="d" placeholder="Description" /></p>
	<button>Upload</button>
</form>
</main>
</body>
</html>