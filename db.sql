-- If anyone asks
-- Asuka is the best girl

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
`id` int(11) NOT NULL COMMENT 'Tracker',
  `name` text NOT NULL COMMENT 'Name of file',
  `location` text NOT NULL COMMENT 'location in the hidden folder',
  `description` text NOT NULL COMMENT 'decription of file',
  `mime` text NOT NULL COMMENT 'MIME type, for propper outputing',
  `filename` text NOT NULL COMMENT 'File name to be used with Content-Disposition'
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='holds files' AUTO_INCREMENT=4 ;

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
`id` int(11) NOT NULL COMMENT 'Tracker',
  `uid` int(11) NOT NULL,
  `uname` text NOT NULL,
  `file` int(11) NOT NULL,
  `ip` text NOT NULL,
  `useragent` text NOT NULL,
  `hash` text NOT NULL,
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
`id` int(11) NOT NULL COMMENT 'Tracker',
  `username` text NOT NULL COMMENT 'Username',
  `password` text NOT NULL COMMENT 'Password',
  `salt` text NOT NULL COMMENT 'Salt',
  `permissions` int(11) NOT NULL COMMENT 'Permissions, RTFM for details.'
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Holds users' AUTO_INCREMENT=9 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `salt`, `permissions`) VALUES
(5, 'admin', '??U/8TNm4fNy2', '?????iU??I?>????@E?#????v', 30);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `files`
--
ALTER TABLE `files`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tracker',AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tracker',AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tracker',AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
